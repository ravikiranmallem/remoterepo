package com.conn;
import java.sql.*;
public class DisplayMetadata {

	
		public static void main(String[] args) {
			try{ 
				//1
				Class.forName("com.mysql.jdbc.Driver");  //driver name
				//Class.forName("oracle.jdbc.driver.OracleDriver");
				//Class.forName("org.postgresql.Driver");
				//2	
				Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/college","root","root");
				//Connection con1=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:db_name","system","oracle");  
				//Connection con2= DriverManager.getConnection("jdbc:postgresql://localhost:5432/example", "postgres", "postgres");
					 
					//here college is database name, root is username and password  
				//3	
				Statement st=con.createStatement();  
				//4	
				//ResultSet rs=stmt.executeQuery("select * from emp");
				ResultSet rs=st.executeQuery(" select dname, sum(salary),max(salary), ename from emp group by dname");
				
				// Get resultset metadata
				// 
				ResultSetMetaData md = rs.getMetaData();
				int columnCount = md.getColumnCount();
				// Get the column names; column indices start from 1
				for (int i=1; i<=columnCount; i++) {
				   String columnName = md.getColumnName(i);
				   System.out.print(columnName+"|");
				}
				 System.out.println("\n");
				//	
				while(rs.next()) {  
					//System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4)+" "+rs.getInt(5));
					System.out.println(rs.getString(1)+"    "+rs.getString(2)+"    "+rs.getString(3)+"    "+rs.getString(4));
				}
				//5
				con.close();  
				
			}
			catch(Exception e){ 
				System.out.println(e);
			}  
		
	}

}
